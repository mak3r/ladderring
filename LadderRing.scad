


rungRadius=.35; //min of .35 for flexible plastic at shapeways
ringSize = 21; //size of the ring
ringRadius=ringSize/2; 
numRungs=32; //change the number of rungs in the ring
rungLength=16; //change this value to change the width of the ring. 
rungAngle=0; 
rimWidth=1.8; //change this to make the outer edge width change
rimDepth=1; //change this to vary the rim depth - from finger to outside
innerDiameter=2*(ringRadius-1); //change this to vary the internal radius (size of the ring)
rimTranslate = ( rungAngle == 0 ? .5*rungLength :  .5*rungLength*cos(rungAngle)-(rimWidth/2) );


translate([0,0,rimTranslate])
rim();
rungs();
translate([0,0,-(rimWidth+rimTranslate)])
rim();

module rim($fn=80) {
	difference() {
		cylinder(r=ringRadius+rimDepth+.6,h=rimWidth);
		translate([0,0,-1])
		cylinder(r=innerDiameter/2,h=rimWidth+2);
	}
}

module rungs($fn=16) {
	for ( i = [0 : numRungs] ) {
		rotate(i*360/numRungs, [0,0,1])
		rotate(rungAngle, [1,0,0])
		translate([ringRadius,0,0])
		translate([0,0,-rungLength/2])
		cylinder(r=rungRadius, h=rungLength);
	}
}

